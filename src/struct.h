#define WIDTH 512
#define HEIGHT 512

typedef struct color_s {
  unsigned char r, g, b, a;
} color_t;

typedef struct area_s {
  int xs, ys, xe, ye;
} area_t;

typedef struct quadtree_s {
  double error;            
  struct color_s *color;
  struct area_s *area;
  struct quadtree_s *NO, *NE, *SO, *SE;
} quadtree_t;