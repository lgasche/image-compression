#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <MLV/MLV_all.h>

#include "pixel.h"
#include "quadtree.h"
#include "compression.h"
#include "graphics.h"

int main() {
  char *image_name = " ";
  quadtree_t *qt = NULL;
  color_t pixel_array[WIDTH][HEIGHT];
  MLV_Image *image;

  screen_menu(&image_name);

  /* Init_pixel_array. */
  MLV_create_window("Init array pixels", "Init array pixels", WIDTH, HEIGHT); 
  image = MLV_load_image(image_name);
  init_pixel_array(image, pixel_array);
  MLV_free_image(image);
  MLV_free_window();
  image_preview(image_name);

  MLV_create_window("Compression", "compression", WIDTH, HEIGHT); 

  /* Init quadtree. */
  double error = 100;
  int xs = 0, ys = 0, xe = WIDTH, ye = HEIGHT;
  int i = 0;
  quadtree_t *save_qt = NULL;

  do {
    /* Initializes local variables. */
    xs = 0; ys = 0; xe = WIDTH; ye = HEIGHT;
    save_qt = NULL;

    /* Find the leaf with the biggest error. */
    find_biggest_error(qt, &save_qt, xs, ys, xe, ye, &xs, &ys, &xe, &ye);
    if(i > 1){
      draw_rectangle(qt);
    }
    i++;
  } while (subdiv(save_qt == NULL ? &qt : &save_qt, pixel_array, xs, ys, xe, ye, error));
  print_qt(qt, "final qt");
  MLV_wait_seconds(3);
  MLV_free_window();
  free_qt(qt);
  return 0;
}
