#include "pixel.h"

void init_pixel_array(MLV_Image *image, color_t array[WIDTH][HEIGHT]) {
  int i, j;
  int r, g, b, a;
  for(i = 0; i < WIDTH; i+=1) {
    for(j = 0; j < HEIGHT; j+=1) {
      MLV_get_pixel_on_image(image, i, j, &r, &g, &b, &a);
      array[i][j].r = r;
      array[i][j].g = g;
      array[i][j].b = b;
      array[i][j].a = a;
    }
  }
}

void print_pixel_array_debug(color_t pixel_array[WIDTH][HEIGHT]) {
  int i, j;
  for(i = 0; i < WIDTH; i+=1) {
    for(j = 0; j < HEIGHT; j+=1) {
      printf("red : %d | green : %d | blue %d | alpha %d\n", pixel_array[i][j].r, pixel_array[i][j].g, pixel_array[i][j].b, pixel_array[i][j].a);
    }
  }
}
