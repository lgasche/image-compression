#ifndef _PIXEL_H
#define _PIXEL_H

#include <MLV/MLV_all.h>

#include "struct.h"

/**
 * Takes as parameters an MLV image, and a two-dimensional of color_t.
 * Initialize all the cells of the array [x] [y] with the colors of the pixel of the MLV image.
 */ 
void init_pixel_array(MLV_Image *image, color_t array[WIDTH][HEIGHT]);

void print_pixel_array_debug(color_t pixel_array[WIDTH][HEIGHT]);

#endif /* _PIXEL_H */