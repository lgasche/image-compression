#include <stdlib.h>

#include "compression.h"

void find_biggest_error(quadtree_t *qt, quadtree_t **save_qt, int xs, int ys, int xe, int ye, int* mxs, int* mys, int* mxe, int* mye) {
  if (qt == NULL) {
    return;
  }
  /* Case node. */
  if (qt->NO != NULL) {
    int mx = (xs + xe) / 2, my = (ys + ye) / 2;
    find_biggest_error(qt->NO, save_qt, xs, ys, mx, my, mxs, mys, mxe, mye);
    find_biggest_error(qt->NE, save_qt, mx, ys, xe, my, mxs, mys, mxe, mye);
    find_biggest_error(qt->SO, save_qt, xs, my, mx, ye, mxs, mys, mxe, mye);
    find_biggest_error(qt->SE, save_qt, mx, my, xe, ye, mxs, mys, mxe, mye);
  }
  /* Case leaf. */
  /* If it has a bigger error we save it. */
  else if (*save_qt == NULL || qt->error > (*save_qt)->error) {
    /* Save. */
    *save_qt = qt;
    *mxs = xs; *mxe = xe;
    *mys = ys; *mye = ye;
  }
}

int subdiv(quadtree_t **qt, color_t pixel_array[WIDTH][HEIGHT], int xs, int ys, int xe, int ye, int error) {
  /* Case init qt. */
  if (*qt == NULL) {
    *qt = node_init(pixel_array, xs, ys, xe, ye);
    return 1;
  }
  /* End algo : The error value is less than or equal. */
  else if ((*qt)->error <= error) {
    return 0;
  }
  /* Subdivision of the leaf with the biggest error. */
  else {
    int mx = (xs + xe) / 2, my = (ys + ye) / 2;
    (*qt)->NO = node_init(pixel_array, xs, ys, mx, my);
    (*qt)->NE = node_init(pixel_array, mx, ys, xe, my);
    (*qt)->SO = node_init(pixel_array, xs, my, mx, ye);
    (*qt)->SE = node_init(pixel_array, mx, my, xe, ye);
    return 1;
  }
}