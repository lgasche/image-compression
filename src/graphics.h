#ifndef _GRAPHICS_H
#define _GRAPHICS_H

#include <MLV/MLV_all.h>

#include "quadtree.h"

void draw_rectangle(quadtree_t *qt);

void draw_ellipse(quadtree_t *qt);

void image_preview(char * image_name);

void screen_menu(char ** image_name);

int file_existing(char * image_name);

#endif /* _GRAPHICS_H */