#ifndef _COMPRESSION_H
#define _COMPRESSION_H

#include "quadtree.h"

/**
 * Takes as a parameter a quadtree, a save pointer for the sheet with the most errors.
 * As well as the zone information, to allocate the node after.
 * 
 * xs : x_start
 * xe : x_end
 * 
 * mxs : memorised x_start
 */ 
void find_biggest_error(quadtree_t *qt, quadtree_t **save_qt, int xs, int ys, int xe, int ye, int* mxs, int* mys, int* mxe, int* mye);

/**
 * Takes as parameter a quadtree, a pixel array and the coordinates of an area.
 * Checks if the area can be subdivided.
 * If the area cannot be subdivided, 
 * then all leaves in the quadtree have an error less than or equal to the reference error.
 * If true, subdivide the area and return 1, otherwise return 0.
 */ 
int subdiv(quadtree_t **qt, color_t pixel_array[WIDTH][HEIGHT], int xs, int ys, int xe, int ye, int error);

#endif /* _COMPRESSION_H */