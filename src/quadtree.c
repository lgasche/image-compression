#include <stdlib.h>
#include <math.h>

#include "quadtree.h"

float pow_2(float x) {
  return x * x;
}

double dist(color_t* p1, color_t* p2) {
  return sqrt(pow_2(p1->r - p2->r) + pow_2(p1->g - p2->g) + pow_2(p1->b - p2->b) + pow_2(p1->a - p2->a));
}

quadtree_t* node_allocation() {
  color_t* color;
  area_t* area;
  quadtree_t* node;
  if ((color = (color_t*)malloc(sizeof(color_t))) == NULL) {
    return NULL;
  }
  if ((area = (area_t*)malloc(sizeof(area_t))) == NULL) {
    return NULL;
  }
  if ((node = (quadtree_t*)malloc(sizeof(quadtree_t))) == NULL) {
    return NULL;
  }
  node->color = color;
  node->area = area;
  return node;
}

quadtree_t* node_init(color_t array[WIDTH][HEIGHT], int xs, int ys, int xe, int ye) {
  quadtree_t* node;
  double r = 0, g = 0, b = 0, a = 0;
  if ((node = node_allocation()) == NULL) {
    return NULL;
  }
  /* Calculate color. */
  node->color->r = 0;
  node->color->g = 0;
  node->color->b = 0;
  node->color->a = 0;
  /* Color: sum of pixels of the square (xs, ys) -> (xe, ye). */
  for (int i = xs; i < xe; i++) {
    for (int j = ys; j < ye; j++) {
      r += array[i][j].r;
      g += array[i][j].g;
      b += array[i][j].b;
      a += array[i][j].a;     
    }
  }
  /* If color == 0 is that we have xe == xs 
  OR that it is only black so no need for div. */
  if (r != 0 && g != 0 && b != 0) {     // 
    /* nb_pixel of area == ((xe - xs) * (ye - ys)) */
    node->color->r = r / ((xe - xs) * (ye - ys));
    node->color->g = g / ((xe - xs) * (ye - ys));
    node->color->b = b / ((xe - xs) * (ye - ys));
    node->color->a = a / ((xe - xs) * (ye - ys));
  }
  /* Calculate error. */
  node->error = 0.0;
  for (int i = xs; i < xe; i++) {
    for (int j = ys; j < ye; j++) {
      node->error += dist(&array[i][j], node->color);
    }
  }
  if(node->error < 0){
    printf("node->error : %f", node->error);
  }
  /* Set area. */
  node->area->xs = xs;
  node->area->xe = xe;
  node->area->ys = ys;
  node->area->ye = ye;
  node->NO = NULL; node->NE = NULL;
  node->SO = NULL; node->SE = NULL;
  return node;
}

int is_leaf(quadtree_t* qt){
  if(qt->NO == NULL){
    if(qt->NE == NULL){
      if(qt->SO == NULL){
        if(qt->SE == NULL){
          return 1;
        }
      }
    }
  }
  return 0;
}

void free_qt(quadtree_t* qt) {  
  if (qt == NULL)
    return;
  free(qt->color);
  free(qt->area);
  free_qt(qt->NO);
  free_qt(qt->NE);
  free_qt(qt->SO);
  free_qt(qt->SE);
  free(qt);
}

void print_qt_aux(quadtree_t* qt, int i) {
  if (qt != NULL) {
    for (int x = 0; x < i; x++) {
      printf("  ");
    }
    printf("Node %d: %p | %f | r:%d g:%d b:%d a:%d\n", i, qt, qt->error, qt->color->r, qt->color->g, qt->color->b, qt->color->a);
    print_qt_aux(qt->NO, i+1);
    print_qt_aux(qt->NE, i+1);
    print_qt_aux(qt->SO, i+1);
    print_qt_aux(qt->SE, i+1);
  }
}

void print_qt(quadtree_t* qt, char* name) {
  printf("-------%s-------\n", name);
  /* Case qt empty. */
  if (qt == NULL) {
    printf("Empty\n");
  } else {
    print_qt_aux(qt, 0);
  }
  printf("--------------------\n");
}
