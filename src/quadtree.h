#ifndef _QUADTREE_H
#define _QUADTREE_H

#include "pixel.h"

/**
 * Return the square of a float.
 */
float pow_2(float x);

/**
 * Return the distance between two pixels.
 */ 
double dist(color_t* p1, color_t* p2);

/**
 * Allocate the necessary memory space for a node.
 * Return the address of the node, otherwise return null.
 */
quadtree_t* node_allocation();

/**
 * Takes in parameter an array of color_t and the coordinates of the zone.
 * Initializes a node with the average color of the area, and the coordinates of the area.
 * Return the address of the node, otherwise return NULL.
 */ 
quadtree_t* node_init(color_t array[WIDTH][HEIGHT], int xs, int ys, int xe, int ye);

/**
 * Take a quadtree, check if the quadtree is at a leaf or not
 *and return 1 if node is a leaf else, return 0 
 */
int is_leaf(quadtree_t* qt);

/**
 * Free the memory space occupied by the quadtree.
 */ 
void free_qt(quadtree_t* qt);

/**
 * print a quadtree with an indentation (first node = 0, second node = 1, ...).
 */
void print_qt_aux(quadtree_t* qt, int i);

/**
 * Print a qt named and surrounded by '-'
 */ 
void print_qt(quadtree_t* qt, char* name);

#endif /* _QUADTREE_H */