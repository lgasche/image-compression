#include "graphics.h"

void draw_rectangle(quadtree_t *qt){

  if(is_leaf(qt) == 1){
  	MLV_draw_filled_rectangle(qt->area->xs, qt->area->ys, (qt->area->xe - qt->area->xs), (qt->area->ye - qt->area->ys), MLV_rgba(qt->color->r, qt->color->g, qt->color->b, qt->color->a));

  	MLV_actualise_window();

  }
  else{

	if(qt->NO != NULL){
  		draw_rectangle(qt->NO);
  	}
  	if(qt->NE != NULL){
  		draw_rectangle(qt->NE);
  	}
  	if(qt->SO != NULL){
  		draw_rectangle(qt->SO);
  	}
  	if(qt->SE != NULL){
  		draw_rectangle(qt->SE);
  	}
  
  }
  return;
}


void draw_ellipse(quadtree_t *qt){

  if(is_leaf(qt) == 1){
  	MLV_draw_filled_ellipse(qt->area->xe - qt->area->xs, qt->area->ye - qt->area->ys, (qt->area->xe - qt->area->xs), (qt->area->ye - qt->area->ys), MLV_COLOR_BLACK);
  	MLV_draw_ellipse((qt->area->xe - qt->area->xs), (qt->area->ye - qt->area->ys), (qt->area->xe - qt->area->xs), (qt->area->ye - qt->area->ys), MLV_rgba(qt->color->r, qt->color->g, qt->color->b, qt->color->a));
  	MLV_actualise_window();

  }
  else{

	if(qt->NO != NULL){
  		draw_ellipse(qt->NO);
  	}
  	if(qt->NE != NULL){
  		draw_ellipse(qt->NE);
  	}
  	if(qt->SO != NULL){
  		draw_ellipse(qt->SO);
  	}
  	if(qt->SE != NULL){
  		draw_ellipse(qt->SE);
  	}
  
  }
  return;
}
int file_existing(char * image_name){
	FILE * input_file;
	input_file = fopen(image_name, "r");
	if(input_file == NULL){
		printf("Le fichier n'existe pas\n");
		return 0;
	}
	fclose(input_file);
	return 1;
}

void screen_menu(char ** image_name){
	MLV_create_window("Menu Screen", "MENU SCREEN", WIDTH, HEIGHT);
	MLV_draw_adapted_text_box(WIDTH/2 - 50, 10, "MENU SCREEN", 5, MLV_COLOR_GREEN, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_wait_input_box(30,100,450,90,MLV_COLOR_RED, MLV_COLOR_GREEN, MLV_COLOR_BLACK,"Nom de l'image avec l'extention : ",&(*image_name));
	while(file_existing(*image_name) != 1){
		MLV_draw_adapted_text_box(WIDTH/6, HEIGHT/2 + 100, "Vous vous êtes tromper sur le nom de votre image", 5, MLV_COLOR_RED, MLV_COLOR_RED, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
		MLV_actualise_window();
		MLV_wait_input_box(30,100,450,90,MLV_COLOR_RED, MLV_COLOR_GREEN, MLV_COLOR_BLACK,"Nom de l'image avec l'extention : ",&(*image_name));
		
	}
	MLV_actualise_window();
	MLV_free_window();
}

void image_preview(char * image_name){
	
	if(file_existing(image_name) == 1){

		MLV_create_window("Image preview", "IMAGE PREVIEW", WIDTH,HEIGHT);
		MLV_draw_image(MLV_load_image(image_name),0,0);
		MLV_actualise_window();
		MLV_wait_seconds(3);
		MLV_free_window();

	}

	return;
}